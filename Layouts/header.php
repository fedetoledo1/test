<header>

     <div class="navbar navbar-inverse navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container">
          <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="brand" href="../Layouts/layout.php">Prácticas Profesionalizantes</a>
          <div class="nav-collapse collapse">
            <ul class="nav">
              <li class="active"><a href="../Layouts/layout.php">Inicio</a></li>
              <li><a>Contacto</a></li>
              <li class="dropdown">
                <a  class="dropdown-toggle" data-toggle="dropdown">Ejercicios<b class="caret"></b></a>
                <ul class="dropdown-menu">
                    <li class="nav-header">POO y DB</li>
                    <li class="divider"></li>
                    <li><a href="../alumnos/index.php">Alumnos</a></li>
                    <li><a href="../carreras/index.php">Carreras</a></li>
                    <li><a href="../ajenda/index.php">Agenda</a></li>
                    <li class="divider"></li>
                    <li class="nav-header">Útiles</li>
                    <li><a href="../ejercicios/ejercicio1.php">Pow</a></li>
                    <li><a href="../ejercicios/ejercicio2.php">Área de un triangulo</a></li>
                    <li><a href="../ejercicios/ejercicio3.php">Calculadora</a></li>
                    <li><a href="../ejercicios/ejercicio4.php">Par o impar</a></li>
                    <li><a href="../ejercicios/ejercicio5.php">Mayor</a></li>
                    <li><a href="../ejercicios/ejercicio6.php">Día de la semana</a></li>
                    <li><a href="../ejercicios/ejercicio7.php">Arrays / pares</a></li>
                    <li><a href="../ejercicios/ejercicio9.php">Sueldos</a></li>
                    <li><a href="../ejercicios/ejercicio10.php">Rand con parametros</a></li>
                    
                    <li class="divider"></li>
                    <li class="nav-header">Juegos</li>
                    <li><a href="../ejercicios/dados.php">Dados</a></li>
                  </ul>
              </li>
            </ul>
            <form class="navbar-form pull-right">
              <input class="span2" type="text" placeholder="Email">
              <input class="span2" type="password" placeholder="Password">
              <button type="submit" class="btn">Sign in</button>
              <a class="btn">Login</a>
            
     </form>
          </div>
        </div>
      </div>
    </div>

</header>
