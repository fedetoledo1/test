<!DOCTYPE html>

<html lang="es" xml:lang="es">
<head>
  	<?php 
		require_once('head.php');
	  ?>	
  </head>

<body>

  <header>
  	<?php 
		require_once('header.php');
	  ?>	
  </header>


  <section>	

  	<?php 
			// carga el archivo routing.php para direccionar a la página .php que se incrustará entre la header y el footer
			require_once('main.php');
	   ?>


  </section>



  <footer>
     <br>
  	<?php 
		include_once('footer.php');
  	?>
  </footer>


</body>
</html> 