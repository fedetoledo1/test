<!DOCTYPE html>

<html lang="es" xml:lang="es">
<?php
require_once('../Layouts/head.php');

echo "<body>";

require_once('../Layouts/header.php');
?>

<div class="hero-unit">
          <div class="container-fluid">
          <h1> Ejercicio 1 POW Práctica Prof</h1>
          <br>

          <form name="form1" method="POST" action="ejercicio1.php">
                  <p>
                    <label for="num1">Ingrese cantidad de filas</label>
                    <input type="number" name="num1" require min="1" max="20" value="">
                    <label for="num2">Ingrese cantidad de columnas</label>
                    <input type="number" name="num2" require min="1" max="20" value="">
                    <br>
                   <input type="submit" name="button" id="button">
                </form>
            <?php
                        
            if(isset($_POST["button"])){
              cuadricula ($_POST["num1"],$_POST["num2"]); }

          function cuadricula($num1,$num2)
      {
        
         echo "<div class='container'>";
         echo "<table class='table table-bordered'>";

         for($i=1; $i <= $num1; $i++){
           echo "<tr>";
           for ($j=1; $j <= $num2 ; $j++) { 
             $valcelda= pow($i,$j);
             echo "<td class= 'text-center'> $valcelda </td>";
           }
           echo "</tr>";
         }
         echo "</table>";
         echo "</div>";
      }
         ?>
        </div>

      </div> 

</body>

</html>