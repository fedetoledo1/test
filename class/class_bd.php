<?php

// Clase Base de Datos MySQLi
	  class database{

		const my_server   = 'localhost';  //Servidor
		const my_user     = 'root';       //Usuario
		const my_password = '';           //Contraseña
		const my_db       = 'alumnos';    //Base de datos

		private $my_connection;

// Constructor
		public function __construct(){
               $this->connect();
             }

//Destructor
		public function __destruct(){
               $this->close_connection();
             }

// Conectar
		public function connect(){
		     $this->my_connection = new mysqli(self::my_server, 
			       			                   self::my_user,
    							               self::my_password,
						                       self::my_db);
 // Manejo de errores
               if (mysqli_connect_errno()) {
                    echo "Conexión fallida: " . mysqli_connect_error();
	          exit();
                 }

		}

// Cerrar conexión
		public function close_connection(){
			$this->my_connection->close();
		}


// Consultar (Objetos)		
              public function query($str_sql){

                      $result = $this->my_connection->query($str_sql);
                     
                     if  (!empty($result)) {    
                        $arr_obj = array();
			
			while ($row = $result->fetch_object()){
				$arr_obj[]=$row;
			}

		        $result->close();
			 return $arr_obj;
			}   
        }

// Consulta (Array asociativo)
            public function query_assoc($str_sql){

                $result = $this->my_connection->query($str_sql);
		
               	if  (!empty($result)) {    
			$arr_rows = array();

			while ($row = $result->fetch_assoc()){
				$arr_rows[]=$row;
			}

			$result->close();
			return $arr_rows;
			}
            }

// Ejecutar 	     
            public function execute_query($str_sql){
			$result = $this->my_connection->query($str_sql);

			if ($this->my_connection->errno){
 			    
   		 	 $error = "Error " . $this->my_connection->errno." :" 
                                   . $this->my_connection->error;

		         return $error;
			 exit();	
				
 		         }
			
		}
			
// Obtener el último id + 1
		public function get_last_id($tabla,$id)
		{
			$ultimo_id = 0;
			$sql = "SELECT $id FROM $tabla ORDER BY $id DESC LIMIT 1";
			$rows = $this->query($sql);
			
			foreach($rows as $row)
			{
				$ultimo_id = $row->$id;
			}
			
			if($ultimo_id == 0)
			{$ultimo_id = 1;}
			else
			{$ultimo_id++;}
			
			return $ultimo_id;
		}
		
	}


?>
