<?php
require_once('class_alumno.php');

$agenda = new alumno();

if (isset($_POST['nombre']) && 
	isset($_POST['apellido']) &&
	isset($_POST['carrera']) && 
	isset($_POST['anio'])) {

	$nombre = $_POST['nombre'];
	$apellido = $_POST['apellido'];
	$carrera = $_POST['carrera'];
	$anio = $_POST['anio'];	

	if (isset($_GET['id'])) {
		 $id = $_GET['id'];
		 $agenda->update($id,$nombre,$apellido,$carrera,$anio);
	}
	else{
		$agenda->insert($nombre,$apellido,$carrera,$anio);
	 }
  
}

$agenda->mostrar_tabla();

?>