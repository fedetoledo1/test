<?php
require_once('ajenda.php');

$agenda = new ajenda();

if (isset($_POST['nombre']) && 
	isset($_POST['apellido']) &&
	isset($_POST['telefono']) &&
	isset($_POST['dni']) &&
	isset($_POST['sexo']) &&
	isset($_POST['escuela'])) {

	$nombre = $_POST['nombre'];
	$apellido = $_POST['apellido'];
	$telefono = $_POST['telefono'];
	$dni = $_POST['dni'];
	$sexo = $_POST['sexo'];
	$escuela = $_POST['escuela'];
	

	if (isset($_GET['id'])) {
		 $id = $_GET['id'];
		 $agenda->update($id,$dni,$nombre,$apellido,$sexo,$escuela,$telefono);
	}
	else{
		$agenda->insert($dni,$nombre,$apellido,$sexo,$escuela,$telefono);
	 }
  
}

$agenda->mostrar_tabla();

?>