<?php


class pagina_Web
{
	public function cabecera()
	{
		echo '<html lang="es" xml:lang="es">';
		echo '<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		 <title>Trabajos practicos</title>
		 <meta name="viewport" content="width=device-width, initial-scale=1.0">
		 <link href="../bootstrap/bootstrap.css" rel="stylesheet">
		 <link href="../bootstrap/bootstrap-responsive.css" rel="stylesheet">
		 <link href="https://fonts.googleapis.com/css?family=Righteous&display=swap" rel="stylesheet">
		 <link rel="shortcut icon" href="./bootstrap/cubic.png">
		 <style type="text/css">
		   body {
			 padding-top: 60px;
			 padding-bottom: 40px;
			 
		   }
		 </style>
		 <style type = "text/css">
			.carousel-inner img {
			margin: auto; }
		 </style>
		 <script src="../bootstrap/jquery.js.descarga"></script>
		 <script src="../bootstrap/bootstrap-transition.js.descarga"></script>
		 <script src="../bootstrap/bootstrap-alert.js.descarga"></script>
		 <script src="../bootstrap/bootstrap-modal.js.descarga"></script>
		 <script src="../bootstrap/bootstrap-dropdown.js.descarga"></script>
		 <script src="../bootstrap/bootstrap-scrollspy.js.descarga"></script>
		 <script src="../bootstrap/bootstrap-tab.js.descarga"></script>
		 <script src="../bootstrap/bootstrap-tooltip.js.descarga"></script>
		 <script src="../bootstrap/bootstrap-popover.js.descarga"></script>
		 <script src="../bootstrap/bootstrap-button.js.descarga"></script>
		 <script src="../bootstrap/bootstrap-collapse.js.descarga"></script>
		 <script src="../bootstrap/bootstrap-carousel.js.descarga"></script>
		 <script src="../bootstrap/bootstrap-typeahead.js.descarga"></script>
		 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script> 
	 
	   </head>';
	}

	public function cuerpo()
	{
		echo '<body>';
		require_once('../Layouts/header.php'); 
	} 

	public function pie ( )
	{
		echo '<div id="footer">
      <div class="container">
         <p class="text-muted credit">Pagina de actividades de Federico Toledo - Practicas profesionalizantes</p> 
     </div>
 		</div>';
		echo '</body></html>' ;
	}

	public function mostrar_pagina()
	{
		echo $this->cabecera() ;
		echo $this->cuerpo() ;
		echo $this->pie();
	}

}

class pagina_Web_formulario extends pagina_Web
{ 
	public $action;

	public function formulario_inicio($accion,$method="POST")
	{
		
		echo "<div class=\"container\"><h3 class='brand'>Formularios</h3>";
		echo ("<form class=\"form\" action=\"$accion\" method=\"$method\">");
	}

	public function formulario_fin()
	{
		echo ("</form>");
		echo "</div>";
	}

	public function formulario_label($texto,$input){
		echo "<label for=\"$input\">$texto</label>";
	}

	//public function formulario_date($texto,$nombre,$required="",$value="",$type="date"){}

	public function formulario_caja_texto($texto,$nombre,$required="",$value="",$type="text")
	{
		echo "<div class=\"form-group\">";
		$this->formulario_label($texto,$nombre);
		echo "<input type=\"$type\" class=\"form-control\" name=\"$nombre\" value=\"$value\"";
		if ($required == "X") {
			echo " required ";
		}
		echo "><br>";
		echo "</div>";
	}

	public function formulario_caja_numero($texto,$nombre,$required="",$value="",$type="number")
	{
		echo "<div class=\"form-group\">";
		$this->formulario_label($texto,$nombre);
		echo "<input type=\"$type\" class=\"form-control\" name=\"$nombre\" value=\"$value\"";
		if ($required == "X") {
			echo " required ";
		}
		echo "><br>";
		echo "</div>";
	}

	public function formulario_radio($texto, $nombre, $valor, $selected="",$required=""){
		if ($valor == $selected) {
			$sel = "checked";
		}
		else{
			$sel = "";
		}
		if ($required <> "") {
			$req = "required";
		}
		else{
			$req = "";
		}		
		echo "<div class=\"form-group\">";
    	echo "<input type='radio' name='$nombre' value='$valor' $sel $req>";
    	$this->formulario_label($texto,$nombre);
    	echo "</div>";
	}
	
	public function formulario_boton($value)
	{
		echo "<div class=\"form-group\">";
		echo ("<input type=\"submit\" name=\"Submit\" value = \"$value\" class=\"btn\">" ) ;
	    echo "</div>";		
	}

	



}

 