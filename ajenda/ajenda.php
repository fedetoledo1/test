<?php
require_once('../class/class_bd.php');
require_once('class_web.php');
require_once('../class/class_tabla.php');

class ajenda{

	private $pagina;
	private $formulario;
	private $nombre;
	private $apellido;
	private $telefono;
	private $dni;
	private $sexo;
	private $escuela;
	private $db;

	public function __construct(){
		$this->db = new database;

	}

	public function crear_form($id="")
	{
		$this->formulario = new pagina_Web_formulario("Agenda");		
 
		if ($id <> "") {
 			$this->select_id($id);
		}

		$this->formulario->cabecera();
		$this->formulario->cuerpo();
		if ($id <> "") {
		     $this->formulario->formulario_inicio("index.php?id=$id") ;
			}
		else {
		   $this->formulario->formulario_inicio("index.php") ;
		}
		$this->formulario->formulario_caja_numero("DNI","dni","X", $this->dni);
		$this->formulario->formulario_caja_texto("Nombre","nombre", "X",$this->nombre) ;	
		$this->formulario->formulario_caja_texto("Apellido","apellido","X", $this->apellido);
		$this->formulario->formulario_radio("Masculino","sexo", "M", $this->sexo, "X");
		$this->formulario->formulario_radio("Femenino","sexo", "F", $this->sexo, "X");
		$this->formulario->formulario_caja_texto("Escuela","escuela", "X",$this->escuela) ;
        $this->formulario->formulario_caja_numero("Telefono","telefono","X", $this->telefono);
		$this->formulario->formulario_boton("Guardar");
		$this->formulario->formulario_fin();
		$this->formulario->pie();

    }
    
    public function select_id($id){
		
		$result = $this->db->query("SELECT dni, nombre, apellido, sexo, escuela, telefono FROM alumno WHERE id = $id"); 

		if  (!empty($result)) {
			foreach ($result as $row) {
				
				$this->nombre = $row->nombre;
				$this->dni = $row->dni; 
		        $this->apellido = $row->apellido;
			    $this->sexo = $row->sexo;
				$this->escuela = $row->escuela;
				$this->telefono = $row->telefono;
		       }  
		    }
	}

	public function insert($dni,$nombre,$apellido,$sexo,$escuela,$telefono){
	    $query = "INSERT INTO alumno(dni, nombre, apellido, sexo, escuela, telefono) VALUES ('$dni','$nombre','$apellido','$sexo','$escuela','$telefono') ";
                    
		$result = $this->db->execute_query($query);
	}

	public function update($id,$dni,$nombre,$apellido,$sexo,$escuela,$telefono){
	 $query = "UPDATE alumno SET dni = '$dni', nombre  = '$nombre',  apellido = '$apellido',  sexo  = '$sexo',  escuela  = '$escuela',  telefono  = '$telefono'  WHERE id = $id";
                    
		$result = $this->db->execute_query($query);
	}


	public function delete($id){
		$result = $this->db->execute_query("DELETE FROM alumno WHERE id = $id");
	}

	public function mostrar_tabla(){
		$this->pagina = new pagina_Web("Listado de Alumnos");
		$this->pagina->cabecera();
		$this->pagina->cuerpo();

		echo "<div class='container'><div class='row'><h3 class='text-dark'>Agenda</h3>";
		echo "<a href='form.php' class='btn'>Agendar Alumno</a></div>";
		echo "<div class='row'>";

		$select_all = "SELECT alumno.id, alumno.dni, alumno.nombre, alumno.apellido, alumno.sexo, alumno.escuela, alumno.telefono FROM alumno ORDER BY alumno.apellido";
		$result_select = $this->db->query($select_all); 
		if  (!empty($result_select)) {

			$filas = count($result_select) + 1;
			$tabla1=new tabla($filas,9);
			$i = 1;
			$tabla1->cargar($i,1,"Borrar");
			$tabla1->cargar($i,2,"Editar");
			$tabla1->cargar($i,3,"DNI");
			$tabla1->cargar($i,4,"Nombre");
			$tabla1->cargar($i,5,"Apellido");
			$tabla1->cargar($i,6,"Sexo");
			$tabla1->cargar($i,7,"Escuela");
			$tabla1->cargar($i,8,"Telefono");	
			$tabla1->cargar($i,9,"Inscribir");	

			foreach ($result_select as $row) {
				$i++;
			// SVG: https://icons.getbootstrap.com

			$borrar =  "<a href='delete.php?id=$row->id'>" .  
						'<svg class="bi bi-trash" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
						  <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z"></path>
						  <path fill-rule="evenodd" d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4L4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z"></path>
						</svg>' .  "</a>";
			$tabla1->cargar($i,1,$borrar);
			$editar = "<a href='form.php?id=$row->id'>" . 
							'<svg class="bi bi-pencil" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
							  <path fill-rule="evenodd" d="M11.293 1.293a1 1 0 0 1 1.414 0l2 2a1 1 0 0 1 0 1.414l-9 9a1 1 0 0 1-.39.242l-3 1a1 1 0 0 1-1.266-1.265l1-3a1 1 0 0 1 .242-.391l9-9zM12 2l2 2-9 9-3 1 1-3 9-9z"></path>
							  <path fill-rule="evenodd" d="M12.146 6.354l-2.5-2.5.708-.708 2.5 2.5-.707.708zM3 10v.5a.5.5 0 0 0 .5.5H4v.5a.5.5 0 0 0 .5.5H5v.5a.5.5 0 0 0 .5.5H6v-1.5a.5.5 0 0 0-.5-.5H5v-.5a.5.5 0 0 0-.5-.5H3z"></path>
							</svg></a>' ;
			$tabla1->cargar($i,2,$editar);
			$tabla1->cargar($i,3,$row->dni);
			$tabla1->cargar($i,4,$row->nombre);
			$tabla1->cargar($i,5,$row->apellido);
			$tabla1->cargar($i,6,$row->sexo);
			$tabla1->cargar($i,7,$row->escuela);
			$tabla1->cargar($i,8,$row->telefono);
			$inscribir =  "<a href='../alumnos/form.php?id=$row->id'>" .  
							'<svg class="bi bi-hand-thumbs-up" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
							<path fill-rule="evenodd" d="M6.956 1.745C7.021.81 7.908.087 8.864.325l.261.066c.463.116.874.456 1.012.965.22.816.533 2.511.062 4.51a9.84 9.84 0 0 1 .443-.051c.713-.065 1.669-.072 2.516.21.518.173.994.681 1.2 1.273.184.532.16 1.162-.234 1.733.058.119.103.242.138.363.077.27.113.567.113.856 0 .289-.036.586-.113.856-.039.135-.09.273-.16.404.169.387.107.819-.003 1.148a3.163 3.163 0 0 1-.488.901c.054.152.076.312.076.465 0 .305-.089.625-.253.912C13.1 15.522 12.437 16 11.5 16v-1c.563 0 .901-.272 1.066-.56a.865.865 0 0 0 .121-.416c0-.12-.035-.165-.04-.17l-.354-.354.353-.354c.202-.201.407-.511.505-.804.104-.312.043-.441-.005-.488l-.353-.354.353-.354c.043-.042.105-.14.154-.315.048-.167.075-.37.075-.581 0-.211-.027-.414-.075-.581-.05-.174-.111-.273-.154-.315L12.793 9l.353-.354c.353-.352.373-.713.267-1.02-.122-.35-.396-.593-.571-.652-.653-.217-1.447-.224-2.11-.164a8.907 8.907 0 0 0-1.094.171l-.014.003-.003.001a.5.5 0 0 1-.595-.643 8.34 8.34 0 0 0 .145-4.726c-.03-.111-.128-.215-.288-.255l-.262-.065c-.306-.077-.642.156-.667.518-.075 1.082-.239 2.15-.482 2.85-.174.502-.603 1.268-1.238 1.977-.637.712-1.519 1.41-2.614 1.708-.394.108-.62.396-.62.65v4.002c0 .26.22.515.553.55 1.293.137 1.936.53 2.491.868l.04.025c.27.164.495.296.776.393.277.095.63.163 1.14.163h3.5v1H8c-.605 0-1.07-.081-1.466-.218a4.82 4.82 0 0 1-.97-.484l-.048-.03c-.504-.307-.999-.609-2.068-.722C2.682 14.464 2 13.846 2 13V9c0-.85.685-1.432 1.357-1.615.849-.232 1.574-.787 2.132-1.41.56-.627.914-1.28 1.039-1.639.199-.575.356-1.539.428-2.59z"/>
						  </svg>' .  "</a>";
			$tabla1->cargar($i,9,$inscribir);
			}
			$tabla1->graficar("table table-dark");
		}
      
      echo "</div></div>";
	  $this->pagina->pie();
	}

 }